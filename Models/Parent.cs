﻿using System;
using OTUS.prototype.Interfaces;

namespace OTUS.prototype.Models
{
    public class Parent : GrandParent, IMyCloneable<Parent>
    {
        protected bool MustWork;

        public Parent(string fullName, int age) : base(fullName, age)
        {
            Energy = 6;
            MustWork = true;
        }

        private Parent(Parent instance) : this(instance.FullName, instance.Age){}

        public override Parent MyClone()
        {
            return new Parent(this);
        }

        public override string ToString()
        {
            return $"Parent FullName: {FullName}, Age: {Age}, Energy: {Energy}, MustWork: {MustWork}.{Environment.NewLine}";
        }
    }
}
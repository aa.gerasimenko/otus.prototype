﻿using System;
using OTUS.prototype.Interfaces;

namespace OTUS.prototype.Models
{
    public class Child : Parent, IMyCloneable<Child>
    {
        protected bool HaveFun;

        public Child(string fullName, int age) : base(fullName, age)
        {
            MustWork = false;
            HaveFun = true;
            Energy = 10;
        }

        private Child(Child instance) : this(instance.FullName, instance.Age){}

        public override Child MyClone()
        {
            return new Child(this);
        }

        public override string ToString()
        {
            return $"Parent FullName: {FullName}, Age: {Age}, Energy: {Energy}, MustWork: {MustWork}, HaveFun: {HaveFun}.{Environment.NewLine}";
        }
    }
}
﻿using System;
using OTUS.prototype.Models;

namespace OTUS.prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var grandParent = new GrandParent("Alex Hitch", 35);
            var parent = new Parent("John Hitch", 22);
            var child = new Child("Peter Hitch", 11);

            var grandParentCopy1 = (GrandParent)grandParent.Clone();
            var parentCopy1 = (Parent)parent.Clone();
            var childCopy1 = (Child)child.Clone();

            Console.WriteLine(grandParentCopy1);
            Console.WriteLine(parentCopy1);
            Console.WriteLine(childCopy1);

            var grandParentCopy2 = (GrandParent)grandParentCopy1.Clone();
            var parentCopy2 = (Parent)parentCopy1.Clone();
            var childCopy2 = (Child)childCopy1.Clone();

            grandParentCopy2.FullName += " Changed!";
            parentCopy2.FullName += " Changed!";
            childCopy2.FullName += " Changed!";

            Console.WriteLine(grandParentCopy2);
            Console.WriteLine(parentCopy2);
            Console.WriteLine(childCopy2);

            Console.ReadKey();
        }
    }
}
